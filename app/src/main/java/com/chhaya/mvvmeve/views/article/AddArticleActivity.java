package com.chhaya.mvvmeve.views.article;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.chhaya.mvvmeve.R;
import com.chhaya.mvvmeve.models.api.entities.Article;
import com.chhaya.mvvmeve.models.api.responses.SingleArticleResponse;
import com.chhaya.mvvmeve.viewmodels.ArticleViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddArticleActivity extends AppCompatActivity {

    @BindView(R.id.edit_title) EditText editTitle;
    @BindView(R.id.edit_desc) EditText editDesc;
    @BindView(R.id.button_add) Button btnAdd;

    private ArticleViewModel articleViewModel;
    private int articleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article2);

        // Create dependency
        ButterKnife.bind(this);

        articleId = 0;

        onNewIntent(getIntent());

        articleViewModel = ViewModelProviders.of(this)
                .get(ArticleViewModel.class);
        articleViewModel.setId(articleId);
        articleViewModel.initForAdd();

        if (articleId != 0) {
            articleViewModel.getArticleLive().observe(this, new Observer<SingleArticleResponse>() {
                @Override
                public void onChanged(SingleArticleResponse response) {
                    setupArticleforUpdate(response.getData());
                }
            });

        }

        btnAdd.setOnClickListener(v -> {
            String title = editTitle.getText().toString();
            String desc = editDesc.getText().toString();
            String image = "https://jakewharton.github.io/butterknife/static/logo.png";
            Article article = new Article(image, desc, title);
            if (articleId != 0) {
                // update
                articleViewModel.updateArticle(article);
            } else {
                // insert
                articleViewModel.insertArticle(article);
            }
            finish();
        });

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            articleId = extras.getInt("id");
        }
    }


    private void setupArticleforUpdate(Article article) {
        editTitle.setText(article.getTitle());
        editDesc.setText(article.getDescription());
    }
}
