package com.chhaya.mvvmeve.views.login;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.chhaya.mvvmeve.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {

    private LoginButton loginButton;

    private static final String[] permissions = {"public_profile", "email"};

    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginButton = findViewById(R.id.login_button);

        // TODO implement facebook login
        callbackManager = CallbackManager.Factory.create();

        loginButton.setReadPermissions(Arrays.asList(permissions));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("tag success", "Logged in successfully");
                Log.e("tag success", loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {
                Log.e("tag cancel", "Log in is cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("tag error", error.getMessage());
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
