package com.chhaya.mvvmeve.models.entities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class NotificationObject {

    private String appId;
    private List<String> playerId;
    private String title;
    private String content;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public List<String> getPlayerId() {
        return playerId;
    }

    public void setPlayerId(List<String> playerId) {
        this.playerId = playerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public JSONObject toJsonObject() {
        JSONObject object = new JSONObject();

        try {
            object.put("app_id", getAppId());

            JSONArray playerIds = new JSONArray();
            for (int i = 0; i < getPlayerId().size(); i++) {
                playerIds.put(getPlayerId().get(i));
            }

            object.put("include_player_ids", playerIds);
            object.put("headings",
                    new JSONObject().put("en", getTitle()));
            object.put("contents",
                    new JSONObject().put("en", getContent()));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object;
    }
}
