package com.chhaya.mvvmeve.views.article;

public interface ListArticleCallback {

    void onItemClicked(int position);
    void onDeleteItemClicked(int position);
    void onUpdateItemClicked(int position);

}
