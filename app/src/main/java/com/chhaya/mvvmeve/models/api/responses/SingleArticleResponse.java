package com.chhaya.mvvmeve.models.api.responses;

import com.chhaya.mvvmeve.models.api.entities.Article;
import com.chhaya.mvvmeve.models.api.entities.Pagination;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SingleArticleResponse {

    @SerializedName("PAGINATION")
    private Pagination pagination;
    @SerializedName("DATA")
    private Article data;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("CODE")
    private String code;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public Article getData() {
        return data;
    }

    public void setData(Article data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
