package com.chhaya.mvvmeve.helpers;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginationHelper extends RecyclerView.OnScrollListener {

    private final static int PAGE_LIMIT = 15;

    @NonNull
    private LinearLayoutManager layoutManager;

    public PaginationHelper(@NonNull LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int totalItemCount = layoutManager.getItemCount();
        int visibleItemCount = layoutManager.getChildCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && totalItemCount >= PAGE_LIMIT
                    && firstVisibleItemPosition >= 0) {

                // TODO: load more data
                loadData();

            }
        }

    }

    protected abstract void loadData();
    protected abstract boolean isLoading();
    protected abstract boolean isLastPage();

}
