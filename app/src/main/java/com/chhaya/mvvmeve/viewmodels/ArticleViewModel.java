package com.chhaya.mvvmeve.viewmodels;

import android.util.Log;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.chhaya.mvvmeve.models.api.entities.Article;
import com.chhaya.mvvmeve.models.api.entities.Pagination;
import com.chhaya.mvvmeve.models.api.repoistories.ArticleRepository;
import com.chhaya.mvvmeve.models.api.responses.ListArticlesResponse;
import com.chhaya.mvvmeve.models.api.responses.SingleArticleResponse;

public class ArticleViewModel extends ViewModel {

    // 1. Create live data
    private MutableLiveData<ListArticlesResponse> triggerData; // background
    private MutableLiveData<SingleArticleResponse> article;

    // immutable
    private LiveData<ListArticlesResponse> liveData; // main thread
    private LiveData<SingleArticleResponse> articleLive;

    private ArticleRepository articleRepository;
    private Pagination pagination;
    private int id;

    // 2. Initialization
    public void init() {
        if (liveData != null) {
            return;
        }

        pagination = new Pagination();
        articleRepository = new ArticleRepository();

        triggerData = articleRepository.getListArticleByPagination(pagination);

        // Fire when trigger data is changed
        liveData = Transformations.switchMap(triggerData, new Function<ListArticlesResponse, LiveData<ListArticlesResponse>>() {
            @Override
            public LiveData<ListArticlesResponse> apply(ListArticlesResponse input) {
                Log.d("tag apply", "trigger data is worked");
                return articleRepository.getListArticleByPagination(pagination);
            }
        });

    }

    public void initForAdd() {
        articleRepository = new ArticleRepository();
        article = articleRepository.getArticleById(id);
        articleLive = Transformations.switchMap(article, new Function<SingleArticleResponse, LiveData<SingleArticleResponse>>() {
            @Override
            public LiveData<SingleArticleResponse> apply(SingleArticleResponse input) {
                return articleRepository.getArticleById(id);
            }
        });
    }

    // 3. Create getter for live data
    public LiveData<ListArticlesResponse> getLiveData() {
        return liveData;
    }

    public LiveData<SingleArticleResponse> getArticleLive() {
        return articleLive;
    }

    // allow user to setup pagination
    public Pagination getPagination() {
        return pagination;
    }

    public void getArticles() {
        triggerData.setValue(articleRepository
                .getListArticleByPagination(pagination).getValue());
    }

    // delete article by id
    public void deleteArticleById(int id) {
        articleRepository.deleteArticleById(id);
    }

    // insert an article:
    public void insertArticle(Article article) {
        articleRepository.insertArticle(article);
    }

    // get an article by id:
    public void getArticleById() {
        articleRepository.getArticleById(id);
    }

    // update an article by id:
    public void updateArticle(Article article) {
        articleRepository.updateArticleById(id, article);
    }

    public void setId(int id) {
        this.id = id;
    }

}
