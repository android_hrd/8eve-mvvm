package com.chhaya.mvvmeve.views.onesignal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.chhaya.mvvmeve.R;
import com.chhaya.mvvmeve.helpers.OneSignalHelper;
import com.chhaya.mvvmeve.models.entities.NotificationObject;
import com.onesignal.OneSignal;

import java.util.ArrayList;
import java.util.List;

public class OneSignalActivity extends AppCompatActivity {

    private EditText editMessage;
    private Button btnSendNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_signal);

        editMessage = findViewById(R.id.edit_message);
        btnSendNow = findViewById(R.id.button_send);

        btnSendNow.setOnClickListener(v -> {

            String playerId = OneSignal.getPermissionSubscriptionState()
                    .getSubscriptionStatus().getUserId();

            Log.d("tag player id", playerId);

            // insert into database;

            NotificationObject object = new NotificationObject();
            List<String> playerIds = new ArrayList<>();
            playerIds.add(playerId);

            object.setAppId("07e9d1d0-bfe0-4da7-9985-a1793f06a739");
            object.setPlayerId(playerIds);
            object.setTitle("Congratulation");
            object.setContent(editMessage.getText().toString());

            OneSignalHelper.postNotification(object);
        });

    }
}
