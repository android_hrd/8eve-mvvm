package com.chhaya.mvvmeve.views.article;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.chhaya.mvvmeve.R;
import com.chhaya.mvvmeve.helpers.PaginationHelper;
import com.chhaya.mvvmeve.models.api.responses.ListArticlesResponse;
import com.chhaya.mvvmeve.viewmodels.ArticleViewModel;

public class ListArticleActivity extends AppCompatActivity
    implements ListArticleCallback{

    private RecyclerView rcvListArticle;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ListArticleAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ListArticlesResponse dataSet;

    // Create view model object
    private ArticleViewModel articleViewModel;

    private int currentPage = 1;
    private boolean isReload = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_article);

        rcvListArticle = findViewById(R.id.rcv_list_article);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        setupSwipeRefreshLayout();

        // init vm object
        // This view model becomes view's view model
        articleViewModel = ViewModelProviders.of(this)
                .get(ArticleViewModel.class);
        swipeRefreshLayout.setRefreshing(true);
        articleViewModel.init();

        // Update view when live data is update
        Observer<ListArticlesResponse> observer = new Observer<ListArticlesResponse>() {
            @Override
            public void onChanged(ListArticlesResponse listArticlesResponse) {
                Log.d("tag change", "On Change");
                dataSet = listArticlesResponse;
                setupRecyclerView();
            }
        };

        // Observe live data
        // View is observing to live data
        // View becomes a subscriber of ViewModel
        articleViewModel.getLiveData().observe(this, observer);

    }


    private void setupRecyclerView() {
        if (adapter == null) {
            Log.d("tag", "Init new adapter");
            adapter = new ListArticleAdapter(this, dataSet.getData());
            layoutManager = new LinearLayoutManager(this);
            rcvListArticle.setLayoutManager(layoutManager);
            rcvListArticle.setAdapter(adapter);
        } else if (isReload) {
            // when reload data
            Log.d("tag", "Old adapter is worked");
            adapter.setDataSet(dataSet.getData());
            adapter.notifyDataSetChanged();
            isReload = false;
        } else {
            // when load more data
            adapter.appendDataSet(dataSet.getData());
            adapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setRefreshing(false);

        // add pagination event
        rcvListArticle.addOnScrollListener(new PaginationHelper(layoutManager) {
            @Override
            protected void loadData() {
                // TODO: implement load more data:
                loadMoreData(currentPage++);
                Log.d("tag page", currentPage + "");
            }

            @Override
            protected boolean isLoading() {
                return swipeRefreshLayout.isRefreshing();
            }

            @Override
            protected boolean isLastPage() {
                return currentPage == articleViewModel.getPagination().getTotalPages();
            }
        });
    }

    private void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // TODO: reload data
                isReload = true;
                reloadData();
            }
        });
    }

    private void reloadData() {
        swipeRefreshLayout.setRefreshing(true);
        articleViewModel.getPagination().setPage(1);
        articleViewModel.getArticles();
    }

    private void loadMoreData(int page) {
        swipeRefreshLayout.setRefreshing(true);
        articleViewModel.getPagination().setPage(page);
        articleViewModel.getArticles();
    }


    @Override
    public void onItemClicked(int position) {

    }

    @Override
    public void onDeleteItemClicked(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Confirm")
                .setMessage("Are you sure to delete?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // call delete method
                        int id = adapter.getDataSet().get(position).getId();
                        articleViewModel.deleteArticleById(id);
                        adapter.notifyItemRemoved(position);
                        adapter.getDataSet().remove(position);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // cancel operation
                    }
                });
        builder.create().show();
    }

    @Override
    public void onUpdateItemClicked(int position) {
        Intent updateIntent = new Intent(
                ListArticleActivity.this,
                AddArticleActivity.class
        );
        int id = adapter.getDataSet().get(position).getId();
        updateIntent.putExtra("id", id);
        startActivity(updateIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isReload = true;
        reloadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {
            Intent addIntent = new Intent(
                    ListArticleActivity.this,
                    AddArticleActivity.class
            );
            startActivity(addIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}
