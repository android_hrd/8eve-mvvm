package com.chhaya.mvvmeve.models.api.repoistories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.chhaya.mvvmeve.models.api.config.ServiceGenerator;
import com.chhaya.mvvmeve.models.api.entities.Article;
import com.chhaya.mvvmeve.models.api.entities.Pagination;
import com.chhaya.mvvmeve.models.api.responses.ListArticlesResponse;
import com.chhaya.mvvmeve.models.api.responses.SingleArticleResponse;
import com.chhaya.mvvmeve.models.api.services.ArticleService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    // TODO: implement article repository here
    private ArticleService articleService;

    public ArticleRepository() {
        articleService = ServiceGenerator.createService(ArticleService.class);
    }

    // Get article list:
    public MutableLiveData<ListArticlesResponse> getListArticleByPagination(Pagination pagination) {

        final MutableLiveData<ListArticlesResponse> liveData = new MutableLiveData<>();

        // TODO implement api
        Call<ListArticlesResponse> call =
                articleService.getAll(pagination.getPage(),
                        pagination.getLimit());

        call.enqueue(new Callback<ListArticlesResponse>() {
            @Override
            public void onResponse(Call<ListArticlesResponse> call,
                                   Response<ListArticlesResponse> response) {

                // TODO set response to live data
                if (response.isSuccessful()) {
                    Log.d("tag success", response.body().getMessage());
                    liveData.setValue(response.body());
                }

            }

            @Override
            public void onFailure(Call<ListArticlesResponse> call,
                                  Throwable t) {

                Log.e("tag error", t.getMessage());

            }
        });

        return liveData;
    }

    // Insert an article:
    public MutableLiveData<SingleArticleResponse> insertArticle(Article article) {
        final MutableLiveData<SingleArticleResponse> liveData = new MutableLiveData<>();

        articleService.insertOne(article).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("tag success", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.e("tag fail", t.getMessage());
            }
        });

        return liveData;
    }

    // Get an article by ID:
    public MutableLiveData<SingleArticleResponse> getArticleById(int id) {
        final MutableLiveData<SingleArticleResponse> liveData
                = new MutableLiveData<>();

        articleService.getOne(id).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("tag success", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.e("tag fail", t.getMessage());
            }
        });

        return liveData;
    }

    // Delete an article by ID:
    public MutableLiveData<SingleArticleResponse> deleteArticleById(int id) {
        final MutableLiveData<SingleArticleResponse> liveData
                = new MutableLiveData<>();

        articleService.deleteOne(id).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("tag success", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.e("tag fail", t.getMessage());
            }
        });

        return liveData;
    }

    // Update an article by ID:
    public MutableLiveData<SingleArticleResponse> updateArticleById(int id, Article article) {
        final MutableLiveData<SingleArticleResponse> liveData
                = new MutableLiveData<>();

        articleService.updateOne(id, article).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("tag success", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.e("tag fail", t.getMessage());
            }
        });

        return liveData;
    }

}
