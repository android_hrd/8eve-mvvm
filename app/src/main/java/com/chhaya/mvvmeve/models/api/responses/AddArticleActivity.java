package com.chhaya.mvvmeve.models.api.responses;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.chhaya.mvvmeve.R;
import com.chhaya.mvvmeve.models.api.entities.Article;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddArticleActivity extends AppCompatActivity
    implements View.OnClickListener {

    @BindView(R.id.edit_title) EditText editTitle;
    @BindView(R.id.edit_desc) EditText editDesc;
    @BindView(R.id.button_add) Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);

        ButterKnife.bind(this);

        btnAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_add) {
            // Add article
            String title = editTitle.getText().toString();
            String desc = editDesc.getText().toString();
            String image = "https://jakewharton.github.io/butterknife/static/logo.png";
            Article article = new Article(image, desc, title);

        }
    }
}
