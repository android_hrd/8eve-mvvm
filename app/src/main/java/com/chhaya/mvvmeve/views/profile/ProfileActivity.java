package com.chhaya.mvvmeve.views.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.chhaya.mvvmeve.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}
