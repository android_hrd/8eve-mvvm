package com.chhaya.mvvmeve.helpers;

import android.util.Log;

import com.chhaya.mvvmeve.models.entities.NotificationObject;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class OneSignalHelper {

    public static void postNotification(NotificationObject object) {
        OneSignal.postNotification(object.toJsonObject(), new OneSignal.PostNotificationResponseHandler() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d("tag success", response.toString());
            }

            @Override
            public void onFailure(JSONObject response) {
                Log.e("tag error", response.toString());
            }
        });
    }

}
